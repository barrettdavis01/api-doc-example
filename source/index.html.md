---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - javascript
  - java
  - shell

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - merchants
  - errors

search: true
---

# Introduction

Welcome to the my fake version of API documentation for W2GO API!

We have language bindings in Javascript, Java, Shell! You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right.

All of this is written in markdown and is super easy to maintain. Best way to generate code snippets is to use postman's auto-code generation.

# Authentication

> To authorize, call Auth0 and pray it work for you:

```javascript
var request = require("request");

var options = { method: 'POST',
  url: 'https://dev-ware2goproject.auth0.com/oauth/token',
  headers: 
   { 'cache-control': 'no-cache',
     'Content-Type': 'application/json' },
  body: '{"client_id": "insert_clientId","client_secret": "insert_client_secret","audience": "insert_audience","grant_type": "client_credentials"}' };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

```java
HttpResponse<String> response = Unirest.get("https://staging.ware2goproject.com/api/companies/1/warehouses/1")
  .header("Authorization", "Bearer {insert token}")
  .header("cache-control", "no-cache")
  .asString();
```

> Make sure to replace `client_id` & `client_secret` with your Auth0 key.


`Authorization: Bearer {insert token}`

<aside class="notice">
You must replace <code>client_id</code> with your personal Auth0 key.
</aside>

# Warehouses

## Get All Warehouses

```javascript
var request = require("request");

var options = { method: 'GET',
  url: 'http://localhost:8080/companies/1/warehouses/1',
  headers: 
   { 'cache-control': 'no-cache',
     Authorization: 'Bearer {{bearerToken}}' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": 1,
    "name": "Fluffums",
    "breed": "calico",
    "fluffiness": 6,
    "cuteness": 7
  },
  {
    "id": 2,
    "name": "Max",
    "breed": "unknown",
    "fluffiness": 5,
    "cuteness": 10
  }
]
```

This endpoint retrieves all Warehouses.

### HTTP Request

`GET http://example.com/api/Warehouses`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
include_cats | false | If set to true, the result will also include cats.
available | true | If set to false, the result will include Warehouses that have already been adopted.

<aside class="success">
Remember — a happy Warehouse is an authenticated Warehouse!
</aside>

## Get a Specific Warehouse

```ruby
require 'kittn'

api = Kittn::APIClient.authorize!('meowmeowmeow')
api.Warehouses.get(2)
```

```python
import kittn

api = kittn.authorize('meowmeowmeow')
api.Warehouses.get(2)
```

```shell
curl "http://example.com/api/Warehouses/2"
  -H "Authorization: meowmeowmeow"
```

```javascript
const kittn = require('kittn');

let api = kittn.authorize('meowmeowmeow');
let max = api.Warehouses.get(2);
```

> The above command returns JSON structured like this:

```json
{
  "id": 2,
  "name": "Max",
  "breed": "unknown",
  "fluffiness": 5,
  "cuteness": 10
}
```

This endpoint retrieves a specific Warehouse.

<aside class="warning">Inside HTML code blocks like this one, you can't use Markdown, so use <code>&lt;code&gt;</code> blocks to denote code.</aside>

### HTTP Request

`GET http://example.com/Warehouses/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the Warehouse to retrieve

## Delete a Specific Warehouse

```ruby
require 'kittn'

api = Kittn::APIClient.authorize!('meowmeowmeow')
api.Warehouses.delete(2)
```

```python
import kittn

api = kittn.authorize('meowmeowmeow')
api.Warehouses.delete(2)
```

```shell
curl "http://example.com/api/Warehouses/2"
  -X DELETE
  -H "Authorization: meowmeowmeow"
```

```javascript
const kittn = require('kittn');

let api = kittn.authorize('meowmeowmeow');
let max = api.Warehouses.delete(2);
```

> The above command returns JSON structured like this:

```json
{
  "id": 2,
  "deleted" : ":("
}
```

This endpoint deletes a specific Warehouse.

### HTTP Request

`DELETE http://example.com/Warehouses/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the Warehouse to delete

